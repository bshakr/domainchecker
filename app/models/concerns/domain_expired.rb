module DomainExpired
  extend ActiveSupport::Concern
  
  def check_expiry
    whois = check_domain(self.name)
    self.expiry_date = whois["properties"]["expires_on"]
    save!
  end
  
  private
    def check_domain(domain_name)
      client = RoboWhois.new(:api_key => 'df81f69f0e2698ca5fb84a1c051aef82')
      response = client.whois_properties(domain_name)
    end
end