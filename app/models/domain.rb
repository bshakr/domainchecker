class Domain < ActiveRecord::Base

  include DomainExpired

  validates_presence_of :name
  validates_format_of :name, :with => /\A[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\Z/i , :message => "invalide domain name"

  
end
