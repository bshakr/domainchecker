json.array!(@domains) do |domain|
  json.extract! domain, :name, :expiry_date
  json.url domain_url(domain, format: :json)
end
